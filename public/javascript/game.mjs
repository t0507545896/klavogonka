import { getStoredUsername, clearStoredUsername, moveToRoute } from './utils.mjs';
import { createElement, addClass, removeClass } from "./htmlHelpers.mjs";


const log = console.log;

const CREATE_ROOM_REQUEST_EVENT = 'CREATE_ROOM_REQUEST_EVENT';
const ROOMS_UPDATE_EVENT = 'ROOMS_UPDATE_EVENT';
const JOIN_ROOM_REQUEST_EVENT = 'JOIN_ROOM_REQUEST_EVENT';
const LEAVE_ROOM_REQUEST_EVENT = 'LEAVE_ROOM_REQUEST_EVENT';
const JOIN_ROOM_DONE_EVENT = 'JOIN_ROOM_DONE_EVENT';
const ROOM_PLAYERS_CHANGED_EVENT = 'ROOM_PLAYERS_CHANGED_EVENT';
const PLAYER_READY_FOR_GAME = 'PLAYER_READY_FOR_GAME';
const PLAYER_NOT_READY_FOR_GAME = 'PLAYER_NOT_READY_FOR_GAME';
const ALL_PLAYERS_READY_FOR_GAME = 'ALL_PLAYERS_READY_FOR_GAME';

const username = getStoredUsername();
if (!username) {
  moveToRoute("/login");
} else {
  init(username);
}


function init() {
  const socket = io("http://localhost:3002/rooms", { query: { username } });

  socket.on('connect', () => {
    log(`connected to the Server Socket`);
  });

  setupCreateRoomHandler(socket);
  const joinRoomHandler = createJoinRoomHandler(socket);
  setupRoomsUpdateHandler(socket, joinRoomHandler);


  setRoomsPageVisible(true);
  setGamePageVisible(false);
  
  // socket.on('connect_error', payload => {

  //   alert(payload.message);
  //   clearStoredUsername();
  //   moveToRoute("/login");

  //   console.log(`connect_error ====> `, payload);

  // });

}


function createJoinRoomHandler(socket) {
  socket.on(JOIN_ROOM_DONE_EVENT, payload => {
    log(`Room successfully joined, payload => `, payload);

    const joinedRoom = payload.room;
    initGamePage(socket, joinedRoom);

    setRoomsPageVisible(false);
    setGamePageVisible(true);

    
  });

  return room => {
    log(`Trying to join the Room => `, room);
    socket.emit(JOIN_ROOM_REQUEST_EVENT, { user: username, roomToJoin: room });
  };
}

function initGamePage(socket, joinedRoom) {
  document.getElementById('back-to-rooms-button').onclick = () => {
    const payload = {
      roomToLeave: joinedRoom,
      user: username
    };
    socket.emit(LEAVE_ROOM_REQUEST_EVENT, payload);
    setGamePageVisible(false);
    setRoomsPageVisible(true);
  }

  socket.on(ROOM_PLAYERS_CHANGED_EVENT, payload => {
    log(`ROOM_PLAYERS_CHANGED_EVENT payload => `, payload);
    const { room } = payload;
    renderPlayers(room);
  });


  const gameTextEl = document.getElementById('game-text');
  const readyButtonEl = document.getElementById('game-ready-button');
  readyButtonEl.onclick = () => {
    const isReady = readyButtonEl.innerText.toLowerCase() === 'ready';
    readyButtonEl.innerText = (isReady ? 'not ready' : 'ready').toUpperCase();
  
    const eventType = isReady ? PLAYER_READY_FOR_GAME : PLAYER_NOT_READY_FOR_GAME;
    socket.emit(eventType, { user: username, room: joinedRoom });
  }

  socket.on(ALL_PLAYERS_READY_FOR_GAME, () => {
    setElementVisibility(gameTextEl, true);
    setElementVisibility(readyButtonEl, false);
  })
}

function renderPlayers(room) {
  const playersContainer = document.getElementById('game-users-container');
  playersContainer.innerHTML = '';

  const { users } = room;
  const playerElements = users.map(user => {
    return createPlayerHtmlElement(user);
  });
  playersContainer.append(...playerElements);
}

function createPlayerHtmlElement(player) {
  const { userName, isReady, gameProgress } = player;

  const playerCardContainer = createElement({
    tagName: "div",
    className: "game-user-item",
  });

  const indicatorColor = isReady ? '#A3D200' : 'red';
  const statusIndicatorEl = createElement({
    tagName: "div",
    className: "game-user-status-circle",
    attributes: {
      style: `background: ${indicatorColor};`
    }
  });
  
  const userNameEl = createElement({
    tagName: "p",
    className: "game-user-name",
  });
  userNameEl.innerText = userName === username ? `${userName} (you)` : userName;

  const progressIndEl = createElement({
    tagName: "div",
    className: "user-progress-indicator",
  });

  const progressBarEl = createElement({
    tagName: "div",
    className: "user-progress-bar",
    attributes: {
      style: `width: ${gameProgress}%;`
    }
  });
  progressIndEl.append(progressBarEl);

  playerCardContainer.append(statusIndicatorEl, userNameEl, progressIndEl);

  return playerCardContainer;
}

function setupCreateRoomHandler(socket) {
  const createRoomButton = document.getElementById('create-room-button');
  createRoomButton.onclick = () => {
    const roomNameFromUser = prompt('Please give Room Name');
    const payload = {
      roomName: roomNameFromUser
    };
    socket.emit(CREATE_ROOM_REQUEST_EVENT, payload);
  };
};

function setupRoomsUpdateHandler(socket, joinRoomHandler) {
  socket.on(ROOMS_UPDATE_EVENT, payload => {
    const allRooms = payload.availableRooms;
    if (!allRooms || allRooms.length === 0) {
      return;
    }
    log(`Rooms updated, payload => `, payload);
    renderRooms(allRooms, joinRoomHandler);

  });
} 

/**
 * Renders passed rooms to rooms container.
 */
function renderRooms(rooms, joinRoomHandler) {
  const roomsContainer = document.getElementById('rooms-container');
  roomsContainer.innerHTML = '';

  const roomElements = rooms.map(room => {
    return createRoomHtmlElement(room, joinRoomHandler);
  });
  roomsContainer.append(...roomElements); 
}

/**
 * Visual representation - HTMLElement of the 'room' entity.
 */
function createRoomHtmlElement(room, joinRoomHandler) {
  const { roomName, users } = room;
  const usersInRoom = users.length;

  const roomCardContainer = createElement({
    tagName: "div",
    className: "room-item",
  });

  const usersInRoomEl = createElement({
    tagName: "p",
    className: "users-in-room-label",
  });
  usersInRoomEl.innerText = `${usersInRoom} user connected`;

  const roomNameEl = createElement({
    tagName: "p",
    className: "room-name",
  });
  roomNameEl.innerText = roomName;

  const joinButtonEl = createElement({
    tagName: "button",
    className: "no-select",
    attributes: {
      id: "join-room-button"
    }
  });

  joinButtonEl.innerText = 'Join';

  joinButtonEl.onclick = () => {
    joinRoomHandler(room);
  };

  roomCardContainer.append(usersInRoomEl, roomNameEl, joinButtonEl);

  return roomCardContainer;
}


function setRoomsPageVisible(isVisible = true) {
  const roomsPageEl = document.getElementById('rooms-page');
  const visibility = isVisible ? 'block' : 'none';
  roomsPageEl.style.display = visibility;
}

function setGamePageVisible(isVisible = true) {
  const gamePageEl = document.getElementById('game-page');
  const visibility = isVisible ? 'flex' : 'none';
  gamePageEl.style.display = visibility;
}

function setElementVisibility(htmlElement, isVisible = true) {
  if (isVisible) {
    htmlElement.style.display = 'block';
  } else {
    htmlElement.style.display = 'none';
  }
}