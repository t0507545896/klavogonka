export function getStoredUsername() {
    return sessionStorage.getItem("username");
}

export function clearStoredUsername() {
    sessionStorage.removeItem('username');
}

export function moveToRoute(route) {
    window.location.replace(route);
}
