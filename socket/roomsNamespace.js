import { rememberUser, isUserNameBusy, getUsers } from './auth';
import { Room, rememberRoom, getAllRooms, findRoomByName, UserInRoomWrapper } from './gameData';

/**
 * Returns `username` in case of successful check.
 */
// function checkUserName(socket) {
//     const username = socket.handshake.query.username;
//     if (isUserNameBusy(username)) {
//         socket.emit('connect_error', {
//             message: `User with nickname='${username}' already exists.`
//         });
//         return null;
//     }
//     rememberUser(username);
//     return username;
// }

// const user = checkUserName(socket);
// if (!user) {
//     console.log(`Username: ${user} is busy. Can't connect to the game.`);
//     return;
// }

const CREATE_ROOM_REQUEST_EVENT = 'CREATE_ROOM_REQUEST_EVENT';
const ROOMS_UPDATE_EVENT = 'ROOMS_UPDATE_EVENT';

const JOIN_ROOM_REQUEST_EVENT = 'JOIN_ROOM_REQUEST_EVENT';
const JOIN_ROOM_DONE_EVENT = 'JOIN_ROOM_DONE_EVENT';
const ROOM_PLAYERS_CHANGED_EVENT = 'ROOM_PLAYERS_CHANGED_EVENT';
const LEAVE_ROOM_REQUEST_EVENT = 'LEAVE_ROOM_REQUEST_EVENT';

const PLAYER_READY_FOR_GAME = 'PLAYER_READY_FOR_GAME';
const PLAYER_NOT_READY_FOR_GAME = 'PLAYER_NOT_READY_FOR_GAME';
const ALL_PLAYERS_READY_FOR_GAME = 'ALL_PLAYERS_READY_FOR_GAME';



const log = console.log;
const logE = console.error;

function roomsNamespaceHandler(socketIo) {

    socketIo.on('connection', socket => {

        const id = socket.id;
        log(`Connection established with socketId=${id}`);

        socketIo.clients((error, clients) => {
            if (error) throw error;
            console.log(`All CLIENTS => `, clients);
        });

        socket.emit(ROOMS_UPDATE_EVENT, { availableRooms: getAllRooms() });
        
        socket.on(CREATE_ROOM_REQUEST_EVENT, payload => {
            // payload: { roomName: 'room' }
            log(`CREATE_ROOM_EVENT event received, payload => `, payload);

            const newRoom = new Room(payload.roomName);
            rememberRoom(newRoom);

            const allRoomsPayload = { availableRooms: getAllRooms() };
            socketIo.emit(ROOMS_UPDATE_EVENT, allRoomsPayload);
        });

        socket.on(JOIN_ROOM_REQUEST_EVENT, payload => {
            log(`JOIN_ROOM_REQUEST_EVENT event received, payload => `, payload);
            // payload: { user: 'userTwo', roomToJoin: { roomName: 'one', usersInRoom: 0 } }
            const { user, roomToJoin } = payload;
            const roomName = roomToJoin.roomName;
            const room = findRoomByName(roomName);
            
            if (!room) {
                logE(`No room found with name=${roomName}`);
                return;
            }

            if (room.isUserInRoom(user)) {
                logE(`Player=${user} already in the Room, skip adding him. Room => `, room);    
                return;
            }

            log(`Player=${user} joining Room => `, room);

            socket.join(roomName, () => {
                const wrappedUser = new UserInRoomWrapper(user);
                room.addUser(wrappedUser);
                log(`Room state after user=${user} joined => `, room);
                const allRoomsPayload = { availableRooms: getAllRooms() };
                socketIo.emit(ROOMS_UPDATE_EVENT, allRoomsPayload);

                // Inform single requester initiated room join event about successful join
                socket.emit(JOIN_ROOM_DONE_EVENT, { room: room });
                // Inform Room's participants about players change
                socketIo.to(roomName).emit(ROOM_PLAYERS_CHANGED_EVENT, { room: room });
        
              });
        });

        socket.on(LEAVE_ROOM_REQUEST_EVENT, payload => {
            log(`LEAVE_ROOM_REQUEST_EVENT event received, payload => `, payload);
            // {roomToLeave: joinedRoom, user: username}
            const { roomToLeave, user } = payload;
            const leavingRoomName = roomToLeave.roomName;
            const leavingRoom = findRoomByName(leavingRoomName);
            log(`Player=${user} leaving Room => `, leavingRoom);
    
            if (!leavingRoom) {
                logE(`No room found with name=${leavingRoomName}`);
                return;
            }

            if (!leavingRoom.isUserInRoom(user)) {
                logE(`Player=${user} not found in the Room, skip adding him. Room => `, leavingRoom);    
                return;
            }

            socket.leave(leavingRoomName, () => {

                leavingRoom.removeUser(user);
                log(`Room state after player=${user} leaved => `, leavingRoom);

                const allRoomsPayload = { availableRooms: getAllRooms() };
                socketIo.emit(ROOMS_UPDATE_EVENT, allRoomsPayload);

               // Inform Room's participants about players change
               socketIo.to(leavingRoomName).emit(ROOM_PLAYERS_CHANGED_EVENT, { room: leavingRoom });
            });
        });

        socket.on(PLAYER_READY_FOR_GAME, payload => {
            log(`PLAYER_READY_FOR_GAME event received, payload => `, payload);
            const { user, room: changedRoom } = payload;
            const roomName = changedRoom.roomName;

            const room = findRoomByName(roomName);
            
            if (!room) {
                logE(`No room found with name=${roomName}`);
                return;
            }

            if (!room.isUserInRoom(user)) {
                logE(`Player=${user} not found in the Room  => `, room);    
                return;
            }

            room.setUserReadyForGameStatus(user, true);

            log('Room state => ', room);
    
            socketIo.to(roomName).emit(ROOM_PLAYERS_CHANGED_EVENT, { room: room });
            checkAndStartGameIfAllPlayersReady(room, socket);
        });

        socket.on(PLAYER_NOT_READY_FOR_GAME, payload => {
            log(`PLAYER_NOT_READY_FOR_GAME event received, payload => `, payload);
            const { user, room: changedRoom } = payload;
            const roomName = changedRoom.roomName;

            const room = findRoomByName(roomName);
            
            if (!room) {
                logE(`No room found with name=${roomName}`);
                return;
            }

            if (!room.isUserInRoom(user)) {
                logE(`Player=${user} not found in the Room  => `, room);    
                return;
            }

            room.setUserReadyForGameStatus(user, false);

            log('Room state => ', room);
            
            socketIo.to(roomName).emit(ROOM_PLAYERS_CHANGED_EVENT, { room: room });
            checkAndStartGameIfAllPlayersReady(room, socket);
        });

        

    });

} 

function checkAndStartGameIfAllPlayersReady(room, socket) {

    const playersInRoom = room.users.length;
    if (playersInRoom <= 1) {
        log(`Can't start the game - there are only ${playersInRoom} players`);
        return;
    }

    if (room.isAllPlayersReady()) {
        log('All players ready for the Game');
        socket.emit(ALL_PLAYERS_READY_FOR_GAME);
    }

}


export default roomsNamespaceHandler;

// export default io => {
//     io.on("connection", socket => {

 

//         const allUsers = getUsers();
//         console.log(`Joining user: '${user}' to the rooms. All users: `, allUsers);



//     });
// };