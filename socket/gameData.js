const rooms = [];

export class Room {

    constructor(roomName, users = []) {
        this.roomName = roomName;
        this.users = users;
    }

    isUserInRoom(userName) {
        return this.users.find(wrappedUser => {
            return wrappedUser.userName === userName;
        })
    }

    addUser(userName) {
        this.users.push(userName);
    }

    removeUser(userName) {
        const removeAtIndex = this.users.findIndex(wrappedUser => {
            return wrappedUser.userName === userName;
        });
        if (removeAtIndex !== -1) {
            this.users.splice(removeAtIndex, 1);
        }
    }

    setUserReadyForGameStatus(user, isReady = false) {
        this.users.forEach(wrappedUser => {
            if (wrappedUser.userName === user) {
                wrappedUser.isReady = isReady;
            }
        })
    }

    isAllPlayersReady() {
        return this.users.every(wrappedUser => {
            return wrappedUser.isReady;
        })
    }
    
}

export class UserInRoomWrapper {
    
    constructor(userName, isReady = false, gameProgress = 0) {
        this.userName = userName;
        this.isReady = isReady;
        this.gameProgress = gameProgress;
    }
}

export function rememberRoom(room) {
    rooms.push(room);
}

export function getAllRooms() {
    return rooms;
}

export function findRoomByName(roomNameToFind) {
    const foundRoom = rooms.find(room => {
        return room.roomName === roomNameToFind;
    })
    return foundRoom; // or undefined if nothing found
}