import * as config from "./config";
import rooms from "./roomsNamespace";

export default io => {
  const roomsNamespace = io.of("/rooms");
  rooms(roomsNamespace);
};
