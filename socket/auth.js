const users = new Set();

export function rememberUser(userName) {
    users.add(userName);
}

export function isUserNameBusy(userName) {
    return users.has(userName);
}

export function getUsers() {
    return Array.from(users);
}
